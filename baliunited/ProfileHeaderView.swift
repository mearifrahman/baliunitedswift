//
//  ProfileHeaderView.swift
//  baliunited
//
//  Created by Muhammad Arif Rahman on 1/9/17.
//  Copyright © 2017 swaglord. All rights reserved.
//

import UIKit

class ProfileHeaderView : UIView {
    
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelNumMember: UILabel!
    @IBOutlet weak var labelPoints: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
 
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "ProfileHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
