//
//  ProfileViewController.swift
//  baliunited
//
//  Created by Muhammad Arif Rahman on 1/9/17.
//  Copyright © 2017 swaglord. All rights reserved.
//

import UIKit
import MXParallaxHeader
import MXSegmentedPager

class ProfileViewController : MXSegmentedPagerController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedPager.backgroundColor = UIColor.black
        
        // Parallax Header
        segmentedPager.parallaxHeader.view = ProfileHeaderView.instanceFromNib();
        segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.fill;
        segmentedPager.parallaxHeader.height = 200
        segmentedPager.parallaxHeader.minimumHeight = 20;
        segmentedPager.bounces = false
        
        // Segmented Control customization
        segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down;
        segmentedPager.segmentedControl.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Helvetica", size: 13) as Any]
        segmentedPager.segmentedControl.backgroundColor = UIColor.red
        segmentedPager.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.orange];
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : UIColor(hex:0xfcb61e)]
        segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.textWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.orange
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return ["Merchandrise","Journal"][index];
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
        NSLog("progress %f", parallaxHeader.progress)
    }
    
    
    
}


