//
//  UIView.swift
//  baliunited
//
//  Created by Muhammad Arif Rahman on 1/10/17.
//  Copyright © 2017 swaglord. All rights reserved.
//

import UIKit

extension UIView {
    
    func makeRound(radius:CGFloat = 5) {
        self.layer.cornerRadius = radius
        //        self.clipsToBounds = true
    }
    
}
