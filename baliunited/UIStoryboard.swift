//
//  UIStoryboard.swift
//  baliunited
//
//  Created by Muhammad Arif Rahman on 1/9/17.
//  Copyright © 2017 swaglord. All rights reserved.
//

import UIKit

extension UIStoryboard {
 
    static var profile: UIStoryboard {
        return UIStoryboard(name: "Profile", bundle: nil)
    }
    
}
