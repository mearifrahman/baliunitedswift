//
//  ItemCell.swift
//  baliunited
//
//  Created by Muhammad Arif Rahman on 1/9/17.
//  Copyright © 2017 swaglord. All rights reserved.
//

import UIKit

class ItemCell : UITableViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonRedeem: UIButton!
    @IBOutlet weak var labelPoints: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        buttonRedeem.makeRound()
    }
    
}
